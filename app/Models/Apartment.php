<?php

namespace App\Models;

class Apartment extends BaseModel
{
    protected $fillable = [
        'name',
    ];

    public static function getAll()
    {
        return \QueryFiltersService::query(
            self::class,
            [
                'apartments.*',
            ],
            [
                'search' => [
                    'apartments.name',
                    'apartments.price'
                ],
                'where'  => [
                    'apartments.bedrooms' => 'bedrooms',
                    'apartments.bathrooms' => 'bathrooms',
                    'apartments.storeys' => 'storeys',
                    'apartments.garages' => 'garages',
                ],
            ],
            function ($query, $filters) {
                return $query;
            }
        );
    }
}
