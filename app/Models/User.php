<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'nickname',
        'birthday',
        'email',
        'password',
        'active',
        'avatar',
        'phone',
        'token',
        'email_verified_at'
    ];

    protected $encryptable = [
        'name',
        'nickname',
        'email',
        'phone',
        'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function OauthAccessToken()
    {
        return $this->hasMany('\App\OauthAccessToken');
    }

    public static function registerUser($data)
    {
        $password = $data['password'];

        $data['active'] = 0;
        $data['password'] = bcrypt($password);

        if(!isset($data['phone'])) {
            $data['phone'] = '';
        }

        if(!isset($data['avatar'])) {
            $data['avatar'] = '';
        }

        if(!isset($data['birthday'])) {
            $data['birthday'] = '';
        }

        $user = User::create($data);
        $user->unhash_password = $password;

        return $user;
    }

    public static function updateUser($data)
    {
        $userId = auth('api')->id();

        User::where('id', $userId)->update($data);
    }

    public static function getByEmail($email)
    {
        return User::where('email', $email)->first();
    }
}
