<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class QueryFiltersServiceFacade extends Facade
{
    public static $filters = [];

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'QueryFiltersService';
    }

    public static function filters()
    {
        $filters = request()->get('filters');
        $defaultFilters = [
            'sort' => 'id',
            'order' => 'desc',
            'orderRaw' => '',
            'limit' => 10
        ];

        self::$filters = $filters
            ? array_replace($defaultFilters, json_decode($filters, true))
            : $defaultFilters;

        return self::$filters;
    }

    protected static function query($model, $select, $config, $callback = '', $withoutLimit = false)
    {
        self::filters();

        $model = $model::select($select);

        foreach ($config as $method => $data) {
            self::$method($model, $data);
        }

        $response = (is_callable($callback) ? $callback($model, self::$filters) : $model);

        if(!$withoutLimit) {
            $response = !empty(self::$filters['orderRaw'])
                ? $response->orderByRaw(self::$filters['orderRaw'])
                : $response->orderBy(self::$filters['sort'], self::$filters['order']);
        }

        return (self::$filters['limit'] == 0 || $withoutLimit)
            ? $response->get()
            : $response->paginate(self::$filters['limit']);
    }

    protected static function where($model, $data)
    {
        $model->where(function($query) use($data) {
            foreach ($data as $k => $val) {
                if (array_key_exists($val, self::$filters) && (!is_null(self::$filters[$val]) && self::$filters[$val] !== '')) {
                    self::$filters[$val] === '___null'
                        ? $query->whereNull($k)
                        : $query->where($k, self::$filters[$val]);
                }
            }
        });
    }

    protected static function whereIn($model, $data)
    {
        foreach ($data as $k => $val) {
            if (array_key_exists($val, self::$filters) && (!is_null(self::$filters[$val]) && count(self::$filters[$val]) > 0)) {
                $model->whereIn($k, self::$filters[$val]);
            }
        }
    }

    protected static function whereRaw($model, $data)
    {
        foreach ($data as $k => $val) {
            if (array_key_exists($val, self::$filters) && (!is_null(self::$filters[$val]) && self::$filters[$val] !== '')) {
                $model->whereRaw($k);
            }
        }
    }

    protected static function whereDateTime($model, $data)
    {
        foreach ($data as $k => $val) {
            $k = explode('#', $k);

            if (array_key_exists($val, self::$filters) && !empty(self::$filters[$val])) {
                $model->where($k[0], (isset($k[1]) ? $k[1] : '='), self::$filters[$val]);
            }
        }
    }

    protected static function whereDate($model, $data)
    {
        foreach ($data as $k => $val) {
            $k = explode('#', $k);

            if (array_key_exists($val, self::$filters) && !empty(self::$filters[$val])) {
                $model->whereDate($k[0], (isset($k[1]) ? $k[1] : '='), self::$filters[$val]);
            }
        }
    }

    protected static function whereTime($model, $data)
    {
        foreach ($data as $k => $val) {
            $k = explode('#', $k);

            if (array_key_exists($val, self::$filters) && !empty(self::$filters[$val])) {
                $model->whereTime($k[0], (isset($k[1]) ? $k[1] : '='), self::$filters[$val]);
            }
        }
    }

    protected static function search($model, $data)
    {
        if (array_key_exists('search', self::$filters) && !empty(self::$filters['search'])) {
            $model->where(function ($query) use ($data) {
                foreach ($data as $k => $search) {
                    $query->orWhere($search, 'like', '%' . self::$filters['search'] . '%');
                }
            });
        }
    }
}
