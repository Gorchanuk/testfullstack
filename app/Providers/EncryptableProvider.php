<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EncryptableProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/encryptable.php', 'encryptable');
    }
}
