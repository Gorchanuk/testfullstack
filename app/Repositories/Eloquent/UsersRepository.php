<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;
use Illuminate\Contracts\Hashing\Hasher;
use Cake\Chronos\Date;

/**
 * Class LanguagesRepositoryEloquent
 * @package namespace App\RepositoriesEloquent;
 */
class UsersRepository extends BaseRepository
{
    protected static $getALL = false;

    protected $hasher;

    /**
     * Create a new repository instance.
     *
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @return void
     */
    public function __construct(Hasher $hasher)
    {
        $this->hasher = $hasher;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public static function createUser($data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role_id' => Role::select('id')->where('key', '=', 'user')->first()->id,
            'token' => md5($data['email'])
        ]);

        $user->unhash_password = $data['password'];

        return $user;
    }

    public static function resetPassword($data)
    {
        $pass = str_random(8);

        $user = User::where('email', $data['email'])->first();

        $user->update(['password' => Hash::make($pass)]);

        $user->unhash_password = $pass;

        return $user;
    }

    public function getUserByCredentials()
    {
        $provider = config('auth.guards.api.provider');
        $model = config('auth.providers.'.$provider.'.model');

        if (is_null($model)) {
            response()->error(
                'Unable to determine authentication model from configuration.',
                500
            );
        }

        $data = request()->all();
        $user = (new $model)->where('email', $data['email'])->first();

        if (!$this->hasher->check($data['password'], $user->getAuthPassword())) {
            response()->error('auth.incorrect_password');
        }

        return $user;
    }

    public static function getUserByPassportToken()
    {
        if(isset($_COOKIE['token'])) {
            $tokenGuard = new \Laravel\Passport\Guards\TokenGuard(
                app()->make(\League\OAuth2\Server\ResourceServer::class),
                \Illuminate\Support\Facades\Auth::createUserProvider('users'),
                app()->make(\Laravel\Passport\TokenRepository::class),
                app()->make(\Laravel\Passport\ClientRepository::class),
                app()->make('encrypter')
            );

            $request = \Illuminate\Http\Request::create('/');
            $request->headers->set('Authorization', 'Bearer ' . $_COOKIE['token']);

            return $tokenGuard->user($request);
        }

        return false;
    }

    public static function getCurrent()
    {
        $user = auth('api')->user();
        if(isset($user->id)) {
            $user = User::where('id', $user->id)->with('role')->first();

            $user->register_date = \Jenssegers\Date\Date::parse(new Date($user->created_at))->format('d F Y');

            return $user;
        }

        return null;
    }
}
