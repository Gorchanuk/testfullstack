<?php

namespace App\Http\Controllers\Api;

class ResponseApiController extends \App\Http\Controllers\Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = auth()->guard('api')->user();
    }
}
