<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Requests\Api\v1\Auth\LoginRequest;

class LoginController extends \App\Http\Controllers\Api\ResponseApiController
{
    public function __invoke(LoginRequest $request)
    {
        if(auth()->attempt($request->all())){
            if(!auth()->user()->active) {
                return response()->error('auth.not_active', 401);
            }

            return response()->success(
                \App\Http\Resources\Api\v1\Auth\LoginResource::make(auth()->user())
            );
        }

        return response()->error('auth.invalid_credentials', 401);
    }
}


