<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Requests\Api\v1\Auth\SocialRequest;
use App\Repositories\Eloquent\UserRepository;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends \App\Http\Controllers\Api\ResponseApiController
{
    public function __invoke(SocialRequest $request)
    {
        $data = $request->all();

        //try auth via social network
        try {
            $user = \Socialite::driver($data['type'] . 'Connect')
                ->userByToken($data['token'], (isset($data['secret']) ? $data['secret'] : null));

            $user['auth_type_id'] = \DB::table('auth_types')->where('name', $data['type'])->first()->id;

            return response()->success(
                \App\Http\Resources\Api\v1\Auth\LoginResource::make(UserRepository::createSocial($user))
            );
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
}
