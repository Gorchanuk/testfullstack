<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Requests\Api\v1\Auth\ResetPasswordRequest;
use App\Models\User;
use App\Notifications\Auth\ResetPassword;

class ResetPasswordController extends \App\Http\Controllers\Api\ResponseApiController
{
    public function __invoke(ResetPasswordRequest $request)
    {
        $user = User::getByEmail($request->get('email'));

        if (!$user) {
            return response()->error(trans('auth.check_false_email'));
        }

        $user->notify(
            (new ResetPassword())->locale(app()->getLocale())
        );
    }
}
