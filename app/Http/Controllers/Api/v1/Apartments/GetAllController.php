<?php

namespace App\Http\Controllers\Api\v1\Apartments;

use App\Models\Apartment;

class GetAllController extends \App\Http\Controllers\Api\ResponseApiController
{
    public function __invoke()
    {
        return response()->success(Apartment::getAll());
    }
}


