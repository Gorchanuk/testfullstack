<?php

namespace App\Http\Controllers;

class ViewController extends Controller
{
   public function __invoke()
   {
       return view('app.vue');
   }
}
