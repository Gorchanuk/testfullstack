<?php
namespace App\Http\Requests\Api\v1\Auth;

use App\Http\Requests\BaseApiRequest;

class RegistrationRequest extends BaseApiRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'nickname' => 'string|max:100',
            'email' => 'required|string|email:rfc,filter|max:100|unique:users',
            'password'=> 'required|max:40',
        ];
    }
}
