<?php
namespace App\Http\Requests\Api\v1\Auth;

use App\Http\Requests\BaseApiRequest;

class ResetPasswordRequest extends BaseApiRequest
{
    public function rules()
    {
        return [
            'email'=> 'required|email|string|max:100|exists:users,email',
        ];
    }
}
