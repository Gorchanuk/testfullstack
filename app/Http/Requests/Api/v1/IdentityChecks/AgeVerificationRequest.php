<?php

namespace App\Http\Requests\Api\v1\IdentityChecks;

class AgeVerificationRequest extends \App\Http\Requests\BaseApiRequest
{
    public function rules()
    {
        return [
            'photo' => 'required',
        ];
    }
}
