<?php
namespace App\Http\Requests\Api\v1\Vibes;

use App\Http\Requests\BaseApiRequest;

class CreateRequest extends BaseApiRequest
{
    public function rules()
    {
        return [
            'name'=> 'required|max:200',
        ];
    }
}
