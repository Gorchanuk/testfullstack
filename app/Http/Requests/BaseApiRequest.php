<?php

namespace App\Http\Requests;

abstract class BaseApiRequest extends \Illuminate\Foundation\Http\FormRequest
{
    private $rules = [];

    public function validateResolved()
    {
        if (! $this->passesAuthorization()) {
            $this->failedAuthorization();
        }

        $this->rules = $this->callByMethod('rules');
        $fields = $this->all();

        if (!$fields) {
            response()->error('at_one_field_required', 422);
        }

        $validator = \Validator::make($fields, $this->rules);
        if ($validator->fails()) {
            response()->error([
                'rules' => $this->rules,
                'errors' => $validator->messages()->toArray()
            ]);
        }
    }

    protected function callByMethod($pref)
    {
        return method_exists($this, $pref) ? $this->$pref() : false;
    }
}
