<?php

namespace App\Http\Resources\Api\v1\Auth;

class RegistrationResource extends \Illuminate\Http\Resources\Json\JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'nickname' => $this->nickname,
            'email' => $this->email,
            'avatar' => asset('storage/' . config('upload_files.user_photo.path') . $this->photo),
            'phone' => $this->phone
        ];
    }
}
