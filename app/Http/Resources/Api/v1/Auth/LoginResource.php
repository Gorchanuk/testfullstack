<?php

namespace App\Http\Resources\Api\v1\Auth;

class LoginResource extends \Illuminate\Http\Resources\Json\JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'nickname' => $this->nickname ? $this->nickname : '',
            'email' => $this->email,
            'avatar' => asset('storage/' . config('upload_files.user_photo.path') . $this->avatar),
            'phone' => $this->phone ? $this->phone : '',
            'token' => 'Bearer ' . $this->createToken('MyApp')->accessToken
        ];
    }
}
