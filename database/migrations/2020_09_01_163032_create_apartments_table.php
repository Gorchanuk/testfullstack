<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApartmentsTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->float('price')->default(0);
            $table->tinyInteger('bedrooms')->default(0);
            $table->tinyInteger('bathrooms')->default(0);
            $table->tinyInteger('storeys')->default(0);
            $table->tinyInteger('garages')->default(0);
        });

        $this->callSeed('Apartments');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
