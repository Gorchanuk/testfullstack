<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->binary('name');
            $table->binary('nickname');
            $table->binary('email');
            $table->binary('phone');
            $table->binary('birthday');
            $table->enum('role', ['parent', 'child'])->default('parent');
            $table->string('avatar', 40)->default('');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->boolean('active')->unsigned()->default(1);
            $table->string('token', 254)->nullable();
            $table->rememberToken();
            $this->timestamps($table);
        });

        $this->callSeed('Users');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
