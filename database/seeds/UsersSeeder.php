<?php

class UsersSeeder extends \Illuminate\Database\Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            [
                'name' => 'Jon',
                'nickname' => 'jon.uik',
                'phone' => '',
                'birthday' => '',
                'email' => 'test@gmail.com',
                'password' => bcrypt('1029384756'),
                'active' => 1
            ],
        ]);
    }
}
