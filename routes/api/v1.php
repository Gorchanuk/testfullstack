<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('login', 'LoginController');
    Route::delete('logout', 'LogoutController');
    Route::post('registration', 'RegistrationController');
    Route::post('reset-password', 'ResetPasswordController');
});

Route::group(['prefix' => 'apartments', 'namespace' => 'Apartments'], function () {
    Route::get('get-all', 'GetAllController');
});
