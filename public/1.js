(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _config_components_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../config/components/config */ "./resources/js/config/components/config.js");
/* harmony import */ var _config_components_config__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_config_components_config__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./store */ "./resources/js/components/data_tables/simple/store.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api */ "./resources/js/components/data_tables/simple/api.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_api__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "DataTableSimple",
  props: ['name'],
  data: function data() {
    return {
      firstLoad: true,
      showTable: false,
      options: {},
      searchQuery: "",
      isTyping: false,
      actionColumnWith: 0,
      dynamicColumnWidth: 150
    };
  },
  created: function created() {
    //Register store
    Object(_store__WEBPACK_IMPORTED_MODULE_1__["default"])(this);
    this.load();
    var that = this,
        func = {
      refresh: function refresh(that, filters) {
        that.refreshTable(filters);
      },
      reset: function reset(that) {
        that.loadPage(1);
      }
    };
    this.$root.$on('dataTable' + this.name, function () {
      var method = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'refresh';
      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      func[method](that, data);
    });
  },
  methods: {
    load: function load() {
      var _this = this;

      this.showTable = false;
      _config_components_config__WEBPACK_IMPORTED_MODULE_0___default.a.get(this, 'data_tables/simple').then(function (done) {
        _this.options = done; //Merging filters

        if (_this.options.defFilters) {
          _this.setFilters(_this.options.defFilters);
        }

        _api__WEBPACK_IMPORTED_MODULE_2___default.a.get(_this); // Generate column with

        _this.columnWith();

        _this.firstLoad = false;

        _this.$emit('isInit');
      });
    },
    refreshTable: function refreshTable() {
      var filters = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (filters) {
        this.setFilters(filters);
      }

      _api__WEBPACK_IMPORTED_MODULE_2___default.a.get(this);
    },
    setFilters: function setFilters(filters) {
      if (filters.search) {
        this.searchQuery = filters.search;
      }

      this.$store.dispatch('setToTableFilters', [filters, this.name + 'Filters']);
    },
    loadPage: function loadPage(page) {
      this.$store.dispatch('setToTableFilters', [{
        page: page
      }, this.name + 'Filters']);
      _api__WEBPACK_IMPORTED_MODULE_2___default.a.get(this);
    },
    sizeChange: function sizeChange(limit) {
      localStorage.setItem('tableLimit', limit);
      this.$store.dispatch('setToTableFilters', [{
        limit: limit
      }, this.name + 'Filters']);
      _api__WEBPACK_IMPORTED_MODULE_2___default.a.get(this);
    },
    search: function search(query) {
      this.refreshTable({
        search: query,
        page: 1
      });
    },
    resetSearch: function resetSearch() {
      this.$refs['search-table'].focus();

      if (this.searchQuery !== '') {
        this.searchQuery = '';
        this.setFilters({
          search: '',
          page: 1
        });
        _api__WEBPACK_IMPORTED_MODULE_2___default.a.get(this);
      }
    },
    getFieldValue: function getFieldValue(data, index) {
      index = index.split('.');

      if (index.length > 1) {
        var value;
        index.forEach(function (element, index) {
          value = index === 0 ? data[element] : value[element];
        });
        return value;
      }

      return data[index[0]];
    },
    customField: function customField(data, field, index) {
      return field['func'](data, index, field);
    },
    getColumnWith: function getColumnWith(finld) {
      return finld.width ? finld.width : false;
    },
    columnWith: function columnWith() {
      this.actionColumnWith = this.options.actionColumnWith ? this.options.actionColumnWith : '100';
    }
  },
  computed: {
    dataTable: function dataTable() {
      var store = this.$store.getters[this.name];
      return store.data !== undefined && store.data.length ? store.data : false;
    },
    getCounters: function getCounters() {
      var store = this.$store.getters[this.name];
      return store.counters !== undefined ? store.counters : '';
    },
    getMeta: function getMeta() {
      return this.$store.getters[this.name].meta;
    },
    filters: function filters() {
      return this.$store.getters[this.name + 'Filters'];
    }
  },
  components: {
    Filters: function Filters() {
      return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2)]).then(__webpack_require__.bind(null, /*! ./Filters */ "./resources/js/components/data_tables/simple/Filters.vue"));
    }
  },
  watch: {
    searchQuery: function searchQuery() {
      var _this2 = this;

      setTimeout(function () {
        _this2.isTyping = false;
      }, 1000);
    },
    isTyping: function isTyping(value) {
      if (!value) {
        this.search(this.searchQuery);
      }
    },
    '$route': function $route(route) {
      if (!this.firstLoad && route.name === this.options.forceReloadPage) {
        this.$store.dispatch('resetTableFilters', this.name + 'Filters');
        this.load();
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=template&id=c5457f8e&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=template&id=c5457f8e& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.showTable
    ? _c(
        "div",
        { staticClass: "card table-simple p-0", attrs: { id: _vm.name } },
        [
          _vm.$slots["header"]
            ? _c(
                "div",
                { staticClass: "p-2 card-header" },
                [_vm._t("header")],
                2
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.options.filters ||
          _vm.options.filtersWithoutMenu ||
          _vm.options.search !== false
            ? _c(
                "div",
                {
                  staticClass: "card-header",
                  staticStyle: { padding: "0.40rem 1.25rem", height: "50px" }
                },
                [
                  _c(
                    "el-row",
                    { attrs: { gutter: 24 } },
                    [
                      _vm.options.search !== false
                        ? _c(
                            "el-col",
                            {
                              staticClass: "pl-0",
                              staticStyle: { position: "relative" },
                              attrs: {
                                xl: { span: 6 },
                                lg: { span: 6 },
                                md: { span: 6 },
                                sm: { span: 24 }
                              }
                            },
                            [
                              _c("el-input", {
                                ref: "search-table",
                                attrs: {
                                  type: "text",
                                  placeholder:
                                    _vm.options.search &&
                                    _vm.options.search.placeholder
                                      ? _vm.options.search.placeholder
                                      : "Search"
                                },
                                on: {
                                  input: function($event) {
                                    _vm.isTyping = true
                                  }
                                },
                                model: {
                                  value: _vm.searchQuery,
                                  callback: function($$v) {
                                    _vm.searchQuery = $$v
                                  },
                                  expression: "searchQuery"
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "span",
                                {
                                  staticClass: "search-reset",
                                  on: { click: _vm.resetSearch }
                                },
                                [_vm._v("x")]
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "el-col",
                        {
                          class:
                            _vm.options.search !== false
                              ? "el-col-xl-18 el-col-lg-18 el-col-md-18 el-col-sm-24"
                              : "el-col-xl-24"
                        },
                        [
                          _vm.options.filtersWithoutMenu
                            ? [
                                _c("filters", {
                                  attrs: {
                                    name: _vm.name,
                                    options: _vm.options
                                  }
                                })
                              ]
                            : _vm._e()
                        ],
                        2
                      )
                    ],
                    1
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.$slots["after-filters"]
            ? _c(
                "div",
                { staticClass: "p-2 col-12" },
                [_vm._t("after-filters")],
                2
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.getCounters
            ? _c("div", { staticClass: "card-header" }, [
                _c(
                  "div",
                  { staticClass: "float-right font-weight-bold font-lg" },
                  [_vm._v(_vm._s(_vm.getCounters))]
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.dataTable
            ? _c(
                "div",
                [
                  _c(
                    "el-table",
                    {
                      attrs: {
                        data: _vm.dataTable,
                        "cell-style": { padding: "8px 0" },
                        "row-class-name": _vm.options.rowFunc
                          ? _vm.options.rowFunc
                          : ""
                      }
                    },
                    [
                      _vm._l(_vm.options.fields, function(field, fieldIndex) {
                        return _c("el-table-column", {
                          attrs: {
                            label: field.name,
                            width: _vm.getColumnWith(field),
                            "min-width": _vm.dynamicColumnWidth
                          },
                          scopedSlots: _vm._u(
                            [
                              {
                                key: "default",
                                fn: function(scope) {
                                  return [
                                    field.func
                                      ? [
                                          _c("div", {
                                            domProps: {
                                              innerHTML: _vm._s(
                                                _vm.customField(
                                                  scope.row,
                                                  field,
                                                  fieldIndex
                                                )
                                              )
                                            }
                                          })
                                        ]
                                      : field.component
                                      ? [
                                          _c(field.component, {
                                            tag: "component",
                                            attrs: { data: scope.row }
                                          })
                                        ]
                                      : [
                                          _vm._v(
                                            "\n                        " +
                                              _vm._s(
                                                _vm.getFieldValue(
                                                  scope.row,
                                                  fieldIndex
                                                )
                                              ) +
                                              "\n                    "
                                          )
                                        ]
                                  ]
                                }
                              }
                            ],
                            null,
                            true
                          )
                        })
                      }),
                      _vm._v(" "),
                      _vm.options.actions !== false
                        ? _c("el-table-column", {
                            attrs: {
                              fixed: "right",
                              width: _vm.actionColumnWith
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(scope) {
                                    return [
                                      _c(
                                        "div",
                                        { staticClass: "float-right" },
                                        [
                                          _vm._t("actions", null, {
                                            data: scope.row
                                          })
                                        ],
                                        2
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          })
                        : _vm._e()
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _vm.getMeta
                    ? _c("el-pagination", {
                        staticClass: "mt-2 mb-2",
                        attrs: {
                          "page-size": parseInt(_vm.getMeta.per_page),
                          "page-sizes": [10, 20, 50, 100],
                          "current-page": _vm.getMeta.current_page,
                          total: _vm.getMeta.total,
                          "pager-count": 5,
                          background: "",
                          layout:
                            _vm.getMeta.total > _vm.getMeta.per_page
                              ? "total, prev, pager, next, jumper, sizes"
                              : "total, sizes"
                        },
                        on: {
                          "current-change": _vm.loadPage,
                          "size-change": _vm.sizeChange
                        }
                      })
                    : _vm._e()
                ],
                1
              )
            : _c("div", { staticClass: "text-center mt-5 mb-5" }, [
                _c("h4", [
                  _vm._v(
                    _vm._s(
                      _vm.options.emptyLabel !== undefined
                        ? _vm.options.emptyLabel
                        : "List is empty"
                    )
                  )
                ])
              ])
        ]
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/data_tables/simple/DataTableSimple.vue":
/*!************************************************************************!*\
  !*** ./resources/js/components/data_tables/simple/DataTableSimple.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DataTableSimple_vue_vue_type_template_id_c5457f8e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DataTableSimple.vue?vue&type=template&id=c5457f8e& */ "./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=template&id=c5457f8e&");
/* harmony import */ var _DataTableSimple_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DataTableSimple.vue?vue&type=script&lang=js& */ "./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DataTableSimple_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DataTableSimple_vue_vue_type_template_id_c5457f8e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DataTableSimple_vue_vue_type_template_id_c5457f8e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/data_tables/simple/DataTableSimple.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DataTableSimple_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./DataTableSimple.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DataTableSimple_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=template&id=c5457f8e&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=template&id=c5457f8e& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataTableSimple_vue_vue_type_template_id_c5457f8e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./DataTableSimple.vue?vue&type=template&id=c5457f8e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/data_tables/simple/DataTableSimple.vue?vue&type=template&id=c5457f8e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataTableSimple_vue_vue_type_template_id_c5457f8e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataTableSimple_vue_vue_type_template_id_c5457f8e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/data_tables/simple/api.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/data_tables/simple/api.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports = /*#__PURE__*/function () {
  function ApiTablesSimple() {
    _classCallCheck(this, ApiTablesSimple);
  }

  _createClass(ApiTablesSimple, null, [{
    key: "get",
    value: function get(that) {
      console.log(that.options);
      that.$store.dispatch('SET_LOADING', true);
      var params = {
        filters: that.filters,
        page: that.filters.page
      };

      if (that.options.api.params) {
        params = Object.assign({}, that.options.api.params, params);
      }

      return axios.get('/api/v1/' + that.options.api.path + '/' + that.options.api.method, {
        params: params,
        url_params: that.options.api.url_params ? that.options.api.url_params : []
      }).then(function (ctx) {
        that.$store.dispatch('setToTableList', [ctx.data, that.name]);
        that.showTable = true;
        that.$store.dispatch('SET_LOADING', false);
      });
    }
  }]);

  return ApiTablesSimple;
}();

/***/ }),

/***/ "./resources/js/components/data_tables/simple/store.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/data_tables/simple/store.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (that) {
  var limit = localStorage.getItem('tableLimit');
  var list = {
    state: {},
    getters: {}
  },
      mod = {
    state: {},
    getters: {}
  };

  if (that.$store.state.tables[that.name] === undefined) {
    list.getters[that.name] = function (state) {
      return state;
    };

    that.$store.registerModule(['tables', that.name], list);
  }

  mod.state = {
    search: '',
    page: 1,
    limit: limit ? limit : 10,
    sort: 'id',
    order: 'DESC'
  };

  if (that.$store.state.tables[that.name + 'Filters'] === undefined) {
    mod.getters[that.name + 'Filters'] = function (state) {
      return state;
    };

    that.$store.registerModule(['tables', that.name + 'Filters'], mod);
  }
});

/***/ }),

/***/ "./resources/js/config/components sync recursive ^\\.\\/.*$":
/*!******************************************************!*\
  !*** ./resources/js/config/components sync ^\.\/.*$ ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./config": "./resources/js/config/components/config.js",
	"./config.js": "./resources/js/config/components/config.js",
	"./data_tables/simple/apartments/apartmentsList": "./resources/js/config/components/data_tables/simple/apartments/apartmentsList.js",
	"./data_tables/simple/apartments/apartmentsList.js": "./resources/js/config/components/data_tables/simple/apartments/apartmentsList.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./resources/js/config/components sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./resources/js/config/components/config.js":
/*!**************************************************!*\
  !*** ./resources/js/config/components/config.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports = /*#__PURE__*/function () {
  function Config() {
    _classCallCheck(this, Config);
  }

  _createClass(Config, null, [{
    key: "get",
    value: function get(that, path) {
      return new Promise(function (resolve, reject) {
        resolve(__webpack_require__("./resources/js/config/components sync recursive ^\\.\\/.*$")("./" + path + '/' + that.$route.name + '/' + that.name)['default']);
      });
    }
  }]);

  return Config;
}();

/***/ }),

/***/ "./resources/js/config/components/data_tables/simple/apartments/apartmentsList.js":
/*!****************************************************************************************!*\
  !*** ./resources/js/config/components/data_tables/simple/apartments/apartmentsList.js ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  api: {
    path: 'apartments',
    method: 'get-all'
  },
  search: {
    placeholder: 'Name or Price'
  },
  actions: false,
  filtersWithoutMenu: {
    bedrooms: {
      width: 6,
      label: 'Bedrooms',
      type: 'select',
      data: [{
        id: 1,
        name: 1
      }, {
        id: 2,
        name: 2
      }, {
        id: 3,
        name: 3
      }, {
        id: 4,
        name: 4
      }, {
        id: 5,
        name: 5
      }],
      "default": {
        label: 'All Bedrooms',
        value: null
      }
    },
    bathrooms: {
      width: 6,
      label: 'Bathrooms',
      type: 'select',
      data: [{
        id: 1,
        name: 1
      }, {
        id: 2,
        name: 2
      }, {
        id: 3,
        name: 3
      }, {
        id: 4,
        name: 4
      }],
      "default": {
        label: 'All Bathrooms',
        value: null
      }
    },
    storeys: {
      width: 6,
      label: 'Storeys',
      type: 'select',
      data: [{
        id: 1,
        name: 1
      }, {
        id: 2,
        name: 2
      }, {
        id: 3,
        name: 3
      }, {
        id: 4,
        name: 4
      }],
      "default": {
        label: 'All Storeys',
        value: null
      }
    },
    garages: {
      width: 6,
      label: 'Garages',
      type: 'select',
      data: [{
        id: 1,
        name: 1
      }, {
        id: 2,
        name: 2
      }],
      "default": {
        label: 'All Garages',
        value: null
      }
    }
  },
  fields: {
    name: {
      name: 'Name'
    },
    price: {
      width: 100,
      name: 'Price'
    },
    bedrooms: {
      name: 'Bedrooms'
    },
    bathrooms: {
      name: 'Bathrooms'
    },
    storeys: {
      name: 'Storeys'
    },
    garages: {
      name: 'Garages'
    }
  }
});

/***/ })

}]);