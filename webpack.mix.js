let mix = require('laravel-mix'),
    res = 'resources/';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/bootstrap.js', 'public/js/app')
    .js('resources/js/app/index.js', 'public/js/app')
    .sass('resources/sass/app.sass', 'public/css/app')
    .options({
        processCssUrls: false
    })
    .copy('node_modules/element-ui/lib/theme-chalk/fonts', 'public/fonts')
    .version();
