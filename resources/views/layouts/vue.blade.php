<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>{{ config('app.name') }}</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
        @yield('meta')

        <link id="page_favicon" href="{{ asset('images/favicon.ico') }}" rel="icon" type="image/x-icon">

        @yield('css')
        @yield('styles')

        @yield('scripts-begin')
    </head>
    <body>
        <div id="app"></div>
    </body>

    @yield('js')
</html>
