@foreach($data as $css)
    @if(isset($mix))
        <link rel="stylesheet" href="{{ asset(mix('css/'.$css.'.css')) }}">
    @else
        <link rel="stylesheet" href="{{ asset('css/'.$css.'.css') }}">
    @endif
@endforeach