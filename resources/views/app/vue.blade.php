@extends('layouts.vue')

@section('css')
    @include('entity.css', ['data' => ['app/app'], 'mix' => true])
@endsection

@section('js')
    @include('entity.js', ['data' => ['app/bootstrap', 'app/index']])
@endsection
