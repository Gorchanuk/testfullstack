export default function(that) {
    let limit = localStorage.getItem('tableLimit')

    let list = {
        state: {},
        getters: {},
    },
    mod = {
        state: {},
        getters: {},
    };

    if(that.$store.state.tables[that.name] === undefined) {
        list.getters[that.name] = state => state;
        that.$store.registerModule(['tables', that.name], list);
    }

    mod.state = {
        search: '',
        page: 1,
        limit: limit ? limit : 10,
        sort: 'id',
        order: 'DESC'
    };

    if(that.$store.state.tables[that.name + 'Filters'] === undefined) {
        mod.getters[that.name + 'Filters'] = state => state;
        that.$store.registerModule(['tables', that.name + 'Filters'], mod);
    }
}