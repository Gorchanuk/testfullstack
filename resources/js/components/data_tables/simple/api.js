'use strict';

module.exports = class ApiTablesSimple {
    static get (that) {
        console.log(that.options)

        that.$store.dispatch('SET_LOADING', true);

        let params = {
            filters: that.filters,
            page: that.filters.page
        };

        if (that.options.api.params) {
            params = Object.assign({}, that.options.api.params, params);
        }

        return axios.get('/api/v1/' + that.options.api.path + '/' + that.options.api.method, {
            params: params,
            url_params: that.options.api.url_params ? that.options.api.url_params : []
        }).then((ctx) => {
            that.$store.dispatch('setToTableList', [ctx.data, that.name]);

            that.showTable = true;
            that.$store.dispatch('SET_LOADING', false);
        })
    }
};
