import VueRouter from 'vue-router';

import Full from "../app/containers/Full";
import Apartments from "../app/pages/Apartments";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Full,
            children: [
                {
                    path: '',
                    name: 'apartments',
                    component: Apartments
                }
            ]
        }
    ]
});

export default router;
