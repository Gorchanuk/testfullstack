export default {
    api: {path: 'apartments', method: 'get-all'},
    search: {placeholder: 'Name or Price'},
    actions: false,
    filtersWithoutMenu: {
        bedrooms: {
            width: 6,
            label: 'Bedrooms',
            type: 'select',
            data: [
                {id: 1, name: 1},
                {id: 2, name: 2},
                {id: 3, name: 3},
                {id: 4, name: 4},
                {id: 5, name: 5},
            ],
            default: {label: 'All Bedrooms', value: null}
        },
        bathrooms: {
            width: 6,
            label: 'Bathrooms',
            type: 'select',
            data: [
                {id: 1, name: 1},
                {id: 2, name: 2},
                {id: 3, name: 3},
                {id: 4, name: 4},
            ],
            default: {label: 'All Bathrooms', value: null}
        },
        storeys: {
            width: 6,
            label: 'Storeys',
            type: 'select',
            data: [
                {id: 1, name: 1},
                {id: 2, name: 2},
                {id: 3, name: 3},
                {id: 4, name: 4},
            ],
            default: {label: 'All Storeys', value: null}
        },
        garages: {
            width: 6,
            label: 'Garages',
            type: 'select',
            data: [
                {id: 1, name: 1},
                {id: 2, name: 2},
            ],
            default: {label: 'All Garages', value: null}
        },
    },
    fields: {
        name: {
            name: 'Name',
        },
        price: {
            width: 100,
            name: 'Price',
        },
        bedrooms: {
            name: 'Bedrooms',
        },
        bathrooms: {
            name: 'Bathrooms',
        },
        storeys: {
            name: 'Storeys',
        },
        garages: {
            name: 'Garages',
        },
    }
}
