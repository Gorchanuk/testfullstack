'use strict';

module.exports = class Config {
    static get (that, path) {
        return new Promise(function(resolve, reject) {
            resolve(
                require('./' + path + '/' + that.$route.name+'/' + that.name)['default']
            );
        });
    }
};
