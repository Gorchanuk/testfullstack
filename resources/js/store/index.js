
import Vuex from 'vuex'
import modules from './modules'

const store = new Vuex.Store({
    modules,
    strict: process.env.NODE_ENV !== 'production',
    state: {
        loading: false,
    },
    getters: {
        getLoading: (state) => state.loading,
    },
    actions: {
        SET_LOADING({ commit }, value) {
            commit('SET_LOADING', value)
        }
    },
    mutations: {
        ['SET_LOADING'](state, value) {
            Vue.set(state, 'loading', value)
        },
    },
});
window.store = store

export default store
