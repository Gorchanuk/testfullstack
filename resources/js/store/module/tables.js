export default {
    mutations: {
        setToTableFilters (state, data) {
            state[data[1]] = Object.assign({}, state[data[1]], data[0]);
        },
        resetTableFilters (state, name) {
            let current = state[name];

            state[name] = {
                search: '',
                limit: current.limit,
                sort: current.sort,
                order: current.order
            };
        },
        setToTableList (state, data) {
            state[data[1]] = data[0];
        },
        addToTableList (state, data) {
            data[0].map(function(record) {
                state[data[1]].push(record);
            });
        },
        deleteFromTableList (state, data) {
            state[data[1]].splice(data[0], 1)
        },
    },
    actions: {
        setToTableFilters (context, data) {
            context.commit('setToTableFilters', data);
        },
        resetTableFilters (context, data) {
            context.commit('resetTableFilters', data);
        },
        setToTableList (context, data) {
            context.commit('setToTableList', data);
        },
        addToTableList (context, data) {
            context.commit('addToTableList', data);
        },
        deleteFromTableList (context, data) {
            context.commit('deleteFromTableList', data);
        },
    }
}