window.Vue = require('vue');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['ApiType'] = 'web';

Vue.component('data-table', () => import('./components/data_tables/simple/DataTableSimple'));

//element-ui language
import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'
locale.use(lang);

import {
    Container,
    Main,
    Row,
    Col,
    Input,
    Select,
    Option,
    Notification,
    Loading,
    Table,
    TableColumn,
} from 'element-ui';

Vue.component('el-container', Container);
Vue.component('el-main', Main);
Vue.component('el-row', Row);
Vue.component('el-col', Col);
Vue.component('el-input', Input);
Vue.component('el-select', Select);
Vue.component('el-option', Option);
Vue.component('el-table', Table);
Vue.component('el-table-column', TableColumn);

Vue.use(Loading.directive);

Vue.prototype.$loading = Loading.service;
Vue.prototype.$notify = Notification;

Vue.mixin({
    data() {
        return {
            fullscreenLoading: false,
            progressPercentage: null
        }
    },
    methods: {
        formHandlerSend(name) {
            this.$root.$emit('formHandler' + name + 'Send')
        },
    }
})
