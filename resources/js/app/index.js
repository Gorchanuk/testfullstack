import App from './Index.vue';
import store from '../store';

let router = require('../bootstrap/router')['default'];

new Vue({
	...App,
	router,
    store
}).$mount('#app');
