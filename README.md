##test-fillstack

**Used technologies stack**

1. PHP (Laravel framework, laravel nova, passport, telescope)
2. Mysql (as database)
3. Nginx (as main web server)
4. Redis
5. Docker (for deploy)
6. Apidoc (documentation for api) http://apidocjs.com/

**Install project**

1. Run command # ./system
2. Choose "Run install" 
3. Wait for the installation to complete

**./system**

This is a set of commands for work with "docker"
1. Run Install (First install the project, pull docker containers, settup env, run db migrations and seeds)
2. Run Workspace (Open app:alpine container)
3. Run Docker (Up or down docker containers)
4. Build run build script
5. Deploy run deploy script

All bash scripts in system_scripts
